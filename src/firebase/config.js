import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBaJLbjcAKmx94JKqhhfaK7iwgeFcqf0dM',
  authDomain: 'https://rn-scopic-assignment.firebaseapp.com',
  databaseURL: 'https://rn-scopic-assignment.firebaseio.com',
  projectId: 'rn-scopic-assignment',
  storageBucket: 'rn-scopic-assignment.appspot.com',
  messagingSenderId: '876317162923',
  appId: '1:876317162923:web:d00c908babd3e8eccbe6ee',
};

let Firebase;

if (firebase.apps.length === 0) {
  Firebase = firebase.initializeApp(firebaseConfig);
}

export default Firebase;