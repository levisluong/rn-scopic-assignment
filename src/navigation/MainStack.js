import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { ListScreen, ProfileScreen, WelcomeScreen } from '../screens';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from 'react-native';

const Stack = createStackNavigator();

export default function MainStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Welcome" component={WelcomeScreen} />
            <Stack.Screen name="List" component={ListScreen}
                options={({ navigation }) => ({
                    headerRight: () =>
                        <TouchableOpacity style={{ marginRight: 16 }} onPress={() => navigation.navigate('Profile')}>
                            <Text style={{ color: "#f44336", fontWeight: 'bold' }}>
                                Profile
                            </Text>
                        </TouchableOpacity>

                })} />
            <Stack.Screen name="Profile" component={ProfileScreen} />
        </Stack.Navigator>
    );
}