import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { LoginScreen, RegistrationScreen } from '../screens';

const Stack = createStackNavigator();

export default function AuthStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
            <Stack.Screen name="Registration" component={RegistrationScreen} options={{ headerTitle: 'Sign Up' }} />
        </Stack.Navigator>
    );
}