import React, { useState } from 'react'
import { Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import styles from './styles'

export default function WelcomeScreen({ navigation }) {
    return (
        <View style={styles.container}>
            <View style={styles.wrapperTitle}>
                <Text style={styles.title}>
                    Hi There! Nice to see you.
                </Text>
            </View>

            <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigate('List')}>
                <Text style={styles.buttonTitle}>List</Text>
            </TouchableOpacity>
        </View>
    )
}