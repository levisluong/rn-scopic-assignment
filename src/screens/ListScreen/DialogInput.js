import React, { useEffect, useImperativeHandle, useState } from "react"
import { Button, Dimensions, Modal, View, StyleSheet, TextInput, TouchableOpacity, Text } from "react-native";

const { width } = Dimensions.get("window");

const DialogInput = (props, ref) => {

    const [visible, setVisible] = useState(false);
    const [inputValue, setInputValue] = useState("")

    useImperativeHandle(ref, () => ({
        showDialog() {
            setVisible(true)
        },
    }))

    const toggleModalVisibility = () => {
        setVisible(!visible);
    };

    return (
        <Modal
            transparent
            visible={visible}
            presentationStyle="overFullScreen"
        >
            <View style={styles.viewWrapper}>
                <View style={styles.modalView}>
                    <Text style={{ margin: 16, fontSize: 18, fontWeight: 'bold' }}>
                        Add New Item
                    </Text>
                    <TextInput placeholder="Enter Content Display"
                        value={inputValue} style={styles.textInput}
                        onChangeText={(value) => {
                            if (value.length <= 40) {
                                setInputValue(value)
                            }
                        }} />
                    <Text style={{ alignSelf: 'flex-end', marginRight: 28 }}>
                        {inputValue.length}/40
                    </Text>
                    <View style={{ flexDirection: 'row', margin: 16 }}>
                        <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={() => {
                                toggleModalVisibility()
                                props.onAddItem(inputValue)
                                setInputValue("")
                            }}
                            style={styles.button}>
                            <Text style={{ color: 'white', margin: 15 }}>OK</Text>
                        </TouchableOpacity>
                        <View style={{ width: 16 }} />
                        <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={toggleModalVisibility}
                            style={styles.button}>
                            <Text style={{ color: 'white', margin: 15 }}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    )
}


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#fff",
    },
    viewWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
    modalView: {
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        left: "50%",
        elevation: 5,
        transform: [{ translateX: -(width * 0.4) },
        { translateY: -90 }],
        height: 200,
        width: width * 0.8,
        backgroundColor: "#fff",
        borderRadius: 7,
    },
    button: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        borderBottomWidth: 0,
        borderRadius: 5,
    },
    textInput: {
        width: "90%",
        padding: 4,
        borderRadius: 5,
        borderColor: "rgba(0, 0, 0, 0.2)",
        borderBottomWidth: 1,
    },
});
export default React.forwardRef(DialogInput);