import React, { useContext, useEffect, useRef, useState } from 'react'
import { ActivityIndicator, Animated, FlatList, Keyboard, Text, TextInput, TouchableOpacity, View } from 'react-native'
import styles from './styles';
import Firebase from '../../firebase/config'
import { AuthenticatedUserContext } from "../../navigation/AuthenticatedUserProvider"
import firebase from 'firebase/app'
import { Switch } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { AntDesign } from '@expo/vector-icons';
import DialogInput from './DialogInput';

export default function ListScreen(props) {
    const KEY_DATA_ASYNC = "@key_data_async"

    const { user } = useContext(AuthenticatedUserContext)
    const [isLoading, setIsLoading] = useState(false)
    const refDialog = useRef(null)
    const [entities, setEntities] = useState([])
    const [isUseFirestore, setIsUseFirestore] = useState(true);
    const toggleSwitch = () =>
        setIsUseFirestore(previousState => !previousState);

    const entityRef = Firebase.firestore().collection('entities')

    useEffect(() => {
        console.log()
        setIsLoading(true)
        setEntities([])
        if (isUseFirestore) {
            let unsubcribe = entityRef
                .where("authorID", "==", user.uid)
                .orderBy('createdAt', 'desc')
                .onSnapshot(
                    querySnapshot => {
                        const newEntities = []
                        querySnapshot.forEach(doc => {
                            const entity = doc.data()
                            entity.id = doc.id
                            newEntities.push(entity)
                        });
                        setEntities(newEntities)
                        setIsLoading(false)
                    },
                    error => {
                        console.log(error)
                        setIsLoading(false)
                    }
                )
            return unsubcribe;
        } else {
            AsyncStorage.getItem(KEY_DATA_ASYNC)
                .then(value => {
                    let listEntities = value != null ? JSON.parse(value) : []
                    listEntities = listEntities.filter(element => element.authorID === user.uid)
                    setEntities(listEntities);
                    setIsLoading(false)
                })
                .catch(e => {
                    alert(e)
                    setIsLoading(false)
                })
        }

    }, [isUseFirestore])

    const addItem = (content) => {
        if (content && content.length > 0) {
            const created = firebase.firestore.FieldValue.serverTimestamp();
            const data = {
                text: content,
                authorID: user.uid,
                createdAt: created
            };

            if (isUseFirestore) {
                saveDataToFirestore(data)
            } else {
                saveDataToAsync(data)
            }
        }
    }

    const saveDataToFirestore = async (value) => {
        try {
            await entityRef.add(value)
        } catch (e) {
            alert(error)
        }
    }

    const saveDataToAsync = async (value) => {
        try {
            value.id = (new Date()).getTime
            let listItems = await AsyncStorage.getItem(KEY_DATA_ASYNC)
            listItems = listItems != null ? JSON.parse(listItems) : []
            listItems = listItems.filter(element => element.authorID === user.uid)
            listItems.unshift(value)
            const jsonValue = JSON.stringify(listItems)
            await AsyncStorage.setItem(KEY_DATA_ASYNC, jsonValue)
            setEntities(listItems);
        } catch (e) {
            alert(e)
        }
    }

    const deleteDataToFirestore = async (item) => {
        try {
            await entityRef.doc(item.id).delete()
        } catch (e) {
            alert(e)
        }
    }

    const deleteDataToAsync = async (index) => {
        try {
            let listItems = await AsyncStorage.getItem(KEY_DATA_ASYNC)
            listItems = listItems != null ? JSON.parse(listItems) : []
            listItems.splice(index, 1)
            const jsonValue = JSON.stringify(listItems)
            await AsyncStorage.setItem(KEY_DATA_ASYNC, jsonValue)
            setEntities(listItems);
        } catch (e) {
            alert(e)
        }
    }

    const renderEntity = ({ item, index }) => {
        return (
            <ItemToSwipe itemKey={item.id} data={item.text} handleDelete={() => {
                if (isUseFirestore) {
                    deleteDataToFirestore(item)
                } else {
                    deleteDataToAsync(index)
                }
            }} />
        )
    }

    const ItemToSwipe = (props) => {
        const rightSwipe = (progress, dragX) => {
            const scale = dragX.interpolate({
                inputRange: [0, 100],
                outputRange: [0, 1],
                extrapolate: 'clamp',
            });
            return (
                <TouchableOpacity onPress={props.handleDelete} activeOpacity={0.6}>
                    <View style={styles.deleteBox}>
                        <Animated.Text style={{ color: 'white', fontWeight: 'bold' }}>
                            Delete
                        </Animated.Text>
                    </View>
                </TouchableOpacity>
            );
        };
        return (
            <Swipeable key={props.itemKey} renderRightActions={rightSwipe}>
                <View style={styles.entityContainer}>
                    <Text style={styles.entityText}>
                        {props.data}
                    </Text>
                </View>
            </Swipeable>
        );
    };


    return (
        <View style={styles.container}>
            <View style={{ alignItems: 'center', marginTop: 16 }}>
                <Switch
                    onValueChange={toggleSwitch}
                    value={isUseFirestore}
                />
            </View>

            {isLoading ?
                <ActivityIndicator style={{ margin: 24 }} /> :
                (entities && (
                    <View style={{ flex: 1 }}>
                        <FlatList
                            style={styles.listContainer}
                            data={entities}
                            renderItem={renderEntity}
                            keyExtractor={(item) => item.id}
                            removeClippedSubviews={true}
                        />
                    </View>
                ))
            }

            <TouchableOpacity style={{ position: 'absolute', bottom: 32, right: 32 }} onPress={() => {
                refDialog.current?.showDialog()
            }}>
                <AntDesign name="pluscircle" color={"#f44336"} size={48} />
            </TouchableOpacity>

            <DialogInput ref={refDialog} onAddItem={addItem} />
        </View>
    )
}