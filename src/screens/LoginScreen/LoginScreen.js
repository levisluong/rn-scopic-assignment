import React, { useState } from 'react'
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import Firebase from '../../firebase/config';
import styles from './styles';
import { Entypo } from '@expo/vector-icons';

export default function LoginScreen({ navigation }) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [isShowPassword, setIsShowPassword] = useState(false)
    const onFooterLinkPress = () => {
        navigation.navigate('Registration')
    }

    const onLoginPress = () => {
        if (email !== "" && password !== "") {
            setIsLoading(true)
            Firebase
                .auth()
                .signInWithEmailAndPassword(email, password)
                .then(response => {
                    setIsLoading(false)
                    console.log("Login success")
                    console.log(response)
                })
                .catch(error => {
                    setIsLoading(false)
                    console.log(error)
                    alert(error.message)
                })
        }
    }

    return (
        <View style={styles.container}>
            <Spinner visible={isLoading} />
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, backgroundColor: 'white', width: '100%' }}
                keyboardShouldPersistTaps="always">
                <Image
                    style={styles.logo}
                    source={require('../../../assets/scopic_logo.png')}
                />

                <Text style={styles.title}>
                    Sign In
                </Text>
                <TextInput
                    style={styles.input}
                    placeholder='E-mail'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(text) => setEmail(text)}
                    value={email}
                    keyboardType="email-address"
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                <View>
                    <TextInput
                        style={styles.input}
                        placeholderTextColor="#aaaaaa"
                        secureTextEntry={!isShowPassword}
                        placeholder='Password'
                        pass
                        onChangeText={(text) => setPassword(text)}
                        value={password}
                        underlineColorAndroid="transparent"
                        autoCapitalize="none"
                    />
                    <TouchableOpacity style={{ position: 'absolute', right: 0, top: 20, right: 30 }}
                        onPress={() => {
                            setIsShowPassword(!isShowPassword)
                        }} >
                        <Entypo name={isShowPassword ? "eye-with-line" : "eye"} size={18} color={"#CCCCCC"} />
                    </TouchableOpacity>

                </View>

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => onLoginPress()}>
                    <Text style={styles.buttonTitle}>Sign in</Text>
                </TouchableOpacity>
                <View style={styles.footerView}>
                    <Text style={styles.footerText}>Don't have an account? <Text onPress={onFooterLinkPress} style={styles.footerLink}>Sign up</Text></Text>
                </View>
            </KeyboardAwareScrollView>
        </View>
    )
}