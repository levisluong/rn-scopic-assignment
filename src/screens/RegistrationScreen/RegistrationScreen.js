import React, { useState } from 'react'
import { Alert, Image, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import Firebase from '../../firebase/config';
import styles from './styles';

export default function RegistrationScreen({ navigation }) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [isLoading, setIsLoading] = useState(false)

    const onFooterLinkPress = () => {
        navigation.navigate('Login')
    }

    const onRegisterPress = () => {
        if (email.length == 0) {
            alert("Email not allow empty.")
            return
        }
        if (password.length < 6) {
            alert("Minimum length password is 6.")
            return
        }
        if (password !== confirmPassword) {
            alert("Passwords don't match.")
            return
        }
        setIsLoading(true)
        Firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(response => {
                setIsLoading(false)
            })
            .catch(error => {
                setIsLoading(false)
                alert(error.message)
                console.log(error.message)
            })
    }

    return (
        <View style={styles.container}>
            <Spinner visible={isLoading} />
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, backgroundColor: '#FFFFFF', width: '100%' }}
                keyboardShouldPersistTaps="always">
                <TextInput
                    style={styles.input}
                    placeholder='Email'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(text) => setEmail(text)}
                    value={email}
                    keyboardType="email-address"
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                <TextInput
                    style={styles.input}
                    placeholderTextColor="#aaaaaa"
                    secureTextEntry
                    placeholder='Password'
                    onChangeText={(text) => setPassword(text)}
                    value={password}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                <TextInput
                    style={styles.input}
                    placeholderTextColor="#aaaaaa"
                    secureTextEntry
                    placeholder='Confirm Password'
                    onChangeText={(text) => setConfirmPassword(text)}
                    value={confirmPassword}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => onRegisterPress()}>
                    <Text style={styles.buttonTitle}>Sign Up</Text>
                </TouchableOpacity>
                <View style={styles.footerView}>
                    <Text style={styles.footerText}>Have an account? <Text onPress={onFooterLinkPress} style={styles.footerLink}>Sign in</Text></Text>
                </View>
            </KeyboardAwareScrollView>
        </View>
    )
}