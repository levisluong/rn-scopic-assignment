import React, { useContext } from 'react'
import { Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Firebase from '../../firebase/config'
import { AuthenticatedUserContext } from '../../navigation/AuthenticatedUserProvider'
import styles from './styles'

export default function ProfileScreen({ navigation }) {

    const { user } = useContext(AuthenticatedUserContext)

    return (
        <View style={styles.container}>
            <View style={styles.wrapperTitle}>
                <Text style={styles.title}>
                    {user.email}
                </Text>
            </View>

            <TouchableOpacity
                style={styles.button}
                onPress={() => Firebase.auth().signOut()}>
                <Text style={styles.buttonTitle}>Log Out</Text>
            </TouchableOpacity>
        </View>
    )
}